;;; xc-mode.el --- xC mode derived from CC Mode's c-mode

;; Author:     Martin Schwarz <mschwarz@synapticon.com>
;; Created:    January 2014
;; Version:    0.1
;; Keywords:   xc languages
;; No longer on github!

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with this program; if not, write to the Free Software Foundation, Inc.,
;; 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


;; Differences of xC (compared to plain C)
;; - additional types:     timer, port, chan, chanend, clock
;; -         non-existent: --        
;; - additional modifiers: --
;; -         non-existent: volatile
;; - additional operators: isnull
;; - additional keywords:  select, when, par
;; -         non-existent: goto
;; NOTE: timerafter, pinseq, pinsneq, etc are library functions and
;; not part of the language itself!
;;
;; TODO: find out whether the following needs (or should..or can) be added
;;  - "service", "transaction", "select" functions
;;  - timestamping and timed I/O ("@")
;;  - port buffer width (":N")
;; TODO: compare C vs xC operators (cc-langs.el:907 c-operators vs. xC
;; Programming Guide, page 9))
;; FIXME: add isnull() to the list of operators (?)
;;
;; Also note that the new language features (XMOS Development Tools Version
;; 13 or later) are not yet included

;; see cc-langs.el for variable descriptions ("Lexer-level syntax
;; (identifiers, tokens, etc"))

;; derived-mode-ex.el which is part of the cc-mode package also served
;; as a good starting point


(require 'cc-mode)

;; These are only required at compile time to get the sources for the
;; language constants.  (The cc-fonts require and the font-lock
;; related constants could additionally be put inside an
;; (eval-after-load "font-lock" ...) but then some trickery is
;; necessary to get them compiled.)
(eval-when-compile
  (require 'cc-langs)
  (require 'cc-fonts))

(eval-and-compile
  ;; xC is very similar to C, so we will modify the existing c-mode
  ;; instead of creating a new mode from scratch
  (c-add-language 'xc-mode 'c-mode))

(c-lang-defconst c-assignment-operators
  xc (append '(":>" "<:")
             (c-lang-const c-assignment-operators)))

(c-lang-defconst c-primitive-type-kwds
  xc (append '("timer" "port" "chan" "chanend" "clock")
             (c-lang-const c-primitive-type-kwds)))

(c-lang-defconst c-type-modifier-kwds
  xc (append '("in" "out" "buffered" "streaming")
             (remove "volatile" (c-lang-const c-type-modifier-kwds))))

(c-lang-defconst c-block-stmt-1-kwds
  xc (append '("par" "master" "slave") (c-lang-const c-block-stmt-1-kwds)))

(c-lang-defconst c-block-stmt-2-kwds
  xc (cons "select" (c-lang-const c-block-stmt-2-kwds)))

(c-lang-defconst c-simple-stmt-kwds
  xc (cons "when"
           (remove "goto" (c-lang-const c-simple-stmt-kwds))))

;; replicators "case(int i=0; i<N; i++)"
(c-lang-defconst c-paren-stmt-kwds
  xc (cons "case" (c-lang-const c-paren-stmt-kwds)))

(c-lang-defconst c-before-label-kwds
  xc nil)

;; No bitfields in xC
(c-lang-defconst c-bitfield-kwds
  xc nil)

;; Additional keywords that don't match any other group
;; (c-lang-defconst c-other-kwds
;;   xc nil)

;; this fixes the indendation problem when using the input operater
;; ":>" (which would otherwise be recognized as a label)
(c-lang-defconst c-recognize-colon-labels
  xc nil)

(defcustom xc-font-lock-extra-types nil
  "*List of extra types (aside from the type keywords) to recognize in xC mode.
Each list item should be a regexp matching a single identifier.")

(defconst xc-font-lock-keywords-1 (c-lang-const c-matchers-1 xc)
  "Minimal highlighting for xC mode.")

(defconst xc-font-lock-keywords-2 (c-lang-const c-matchers-2 xc)
  "Fast normal highlighting for xC mode.")

(defconst xc-font-lock-keywords-3 (c-lang-const c-matchers-3 xc)
  "Accurate normal highlighting for xC mode.")

(defvar xc-font-lock-keywords xc-font-lock-keywords-3
  "Default expressions to highlight in xC mode.")

(defvar xc-mode-syntax-table nil
  "Syntax table used in xc-mode buffers.")
(or xc-mode-syntax-table
    (setq xc-mode-syntax-table
	  (funcall (c-lang-const c-make-mode-syntax-table xc))))
  
(defcustom xc-font-lock-extra-types nil
  "*List of extra types (aside from the type keywords) to recognize in xC mode.
Each list item should be a regexp matching a single identifier.")

(defvar xc-mode-abbrev-table nil
  "Abbreviation table used in xc-mode buffers.")
;; (c-define-abbrev-table 'xc-mode-abbrev-table
;;   ;; Keywords that if they occur first on a line might alter the
;;   ;; syntactic context, and which therefore should trig reindentation
;;   ;; when they are completed.
;;   ;; '(("else" "else" c-electric-continued-statement 0)
;;   ;;   ("while" "while" c-electric-continued-statement 0)
;;   ;;   ("catch" "catch" c-electric-continued-statement 0)
;;   ;;   ("finally" "finally" c-electric-continued-statement 0))
;;   )

(defvar xc-mode-map (let ((map (c-make-inherited-keymap)))
		      ;; Add bindings which are only useful for xC
		      map)
  "Keymap used in xc-mode buffers.")

;; (easy-menu-define xc-menu xc-mode-map "xC Mode Commands"
;; 		  ;; Can use `xc' as the language for `c-mode-menu'
;; 		  ;; since its definition covers any language.  In
;; 		  ;; this case the language is used to adapt to the
;; 		  ;; nonexistence of a cpp pass and thus removing some
;; 		  ;; irrelevant menu alternatives.
;;   (cons "xC" (c-lang-const c-mode-menu xc)))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.xc\\'" . xc-mode))

;;;###autoload
(define-derived-mode xc-mode c-mode
  "Major mode for XMOS xC.

The hook `c-mode-common-hook' is run with no args at mode
initialization, then `xc-mode-hook'.

Key bindings:
\\{xc-mode-map}"
  (interactive)
  (kill-all-local-variables)
  (c-initialize-cc-mode t)
  (set-syntax-table xc-mode-syntax-table)
  (setq major-mode 'xc-mode
	mode-name "xC"
	local-abbrev-table xc-mode-abbrev-table
	abbrev-mode t)
  (use-local-map c-mode-map)
  ;; `c-init-language-vars' is a macro that is expanded at compile
  ;; time to a large `setq' with all the language variables and their
  ;; customized values for our language.
  (c-init-language-vars xc-mode)
  ;; `c-common-init' initializes most of the components of a CC Mode
  ;; buffer, including setup of the mode menu, font-lock, etc.
  ;; There's also a lower level routine `c-basic-common-init' that
  ;; only makes the necessary initialization to get the syntactic
  ;; analysis and similar things working.
  (c-common-init 'xc-mode)
  (easy-menu-add xc-menu)
  (run-hooks 'c-mode-common-hook)
  (run-hooks 'xc-mode-hook)
  (c-update-modeline))

(provide 'xc-mode)


