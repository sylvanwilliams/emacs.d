;;User Info
(setq user-full-name "Sylvan Williams")
(setq user-email-address "sylvan@mtns.dev")

;; Install use-package if necessary
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives (append package-archives
			 '(("melpa" . "http://melpa.org/packages/")
			 ("marmalade" . "http://marmalade-repo.org/packages/")
			 ("gnu" . "http://elpa.gnu.org/packages/")
			 ("elpy" . "http://jorgenschaefer.github.io/packages/"))))

(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Enable use-package
(eval-when-compile
  (require 'use-package))
(require 'diminish)                ;; if you use :diminish
(require 'bind-key)                ;; if you use any :bind variant

;; Set the path variable
(use-package exec-path-from-shell
  :ensure t
  :config (exec-path-from-shell-initialize))

;;LSW global prefs
(server-start)
(desktop-save-mode 1)
(setq show-paren-delay 0)
(show-paren-mode 1)
(global-linum-mode t)
(global-company-mode)
(setq desktop-restore-frames t)
(setq desktop-restore-in-current-display t)
(setq desktop-restore-forces-onscreen nil)
(setq calendar-latitude "35.604808")
(setq calendar-longitude "-82.387921")
(setq c-default-style "linux"
      c-basic-offset 4)

(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
    backup-by-copying t    ; Don't delink hardlinks
    version-control t      ; Use version numbers on backups
    delete-old-versions t  ; Automatically delete excess backups
    kept-new-versions 20   ; how many of the newest versions to keep
    kept-old-versions 5    ; and how many of the old
    )
(setq-default indent-tabs-mode nil)

(tool-bar-mode -1)
(scroll-bar-mode -1)

(defadvice show-paren-function
  (after show-matching-paren-offscreen activate)
  "If the matching paren is offscreen, show the matching line in the
    echo area. Has no effect if the character before point is not of
    the syntax class ')'."
  (interactive)
  (let* (
     (cb (char-before (point)))
         (matching-text (and cb
                             (char-equal (char-syntax cb) ?\) )
                             (blink-matching-open)
			     )))))

;;(require 'emacs-color-themes)

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

;;LSW commands
;;; backwards line kill
(defun backward-kill-line (arg)
  "Kill ARG lines backward."
  (interactive "p")
  (kill-line (- 1 arg)))

(defun revert-all-buffers ()
          "Refreshes all open buffers from their respective files"
          (interactive)
          (let* ((list (buffer-list))
                 (buffer (car list)))
            (while buffer
              (when (and (buffer-file-name buffer) 
                         (not (buffer-modified-p buffer)))
                (set-buffer buffer)
                (revert-buffer t t t))
              (setq list (cdr list))
              (setq buffer (car list))))
          (message "Refreshed open files"))

;;LSW keybindings
;(global-set-key "\C-u" 'backward-kill-line)

;;;xc-mode
(add-to-list 'load-path "~/.emacs.d/packages/cc-mode")
(add-to-list 'load-path "~/.emacs.d/packages/xc-mode")
(load "cc-mode")
(load "xc-mode")

;;;ssh-agency
;(add-to-list 'load-path "~/.emacs.d/packages/ssh-agency")
;(load "ssh-agency")

;;; Half-scrolling
(use-package view
  :ensure t
  )
(global-set-key "\C-v" 'View-scroll-half-page-forward)
(global-set-key "\M-v" 'View-scroll-half-page-backward)

(use-package magit
  :init
  (progn
    (use-package magit-blame)
    (bind-key "C-c C-a" 'magit-just-amend magit-mode-map))
  :ensure t
  :bind ("C-x g" . magit-status))

(magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-modules-unpulled-from-upstream
                          'magit-insert-unpulled-from-upstream)
(magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-modules-unpulled-from-pushremote
                          'magit-insert-unpulled-from-upstream)
(magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-modules-unpushed-to-upstream
                          'magit-insert-unpulled-from-upstream)
(magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-modules-unpushed-to-pushremote
                          'magit-insert-unpulled-from-upstream)
(magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-modules-overview
                          'magit-insert-unpulled-from-upstream)
(use-package expand-region
  :bind ("C-=" . er/expand-region))
 
;;;ido
(use-package ido
  :init
  (progn
    (ido-mode 1)
    (use-package ido-vertical-mode
      :ensure t
      :init (ido-vertical-mode 1))
    (use-package flx-ido
      :ensure t
      :init (flx-ido-mode 1))
;    (use-package ido-ubiquitous
;      :ensure t
;      )
    (use-package smex
      :ensure t
      :init (smex-initialize)
      :bind ("M-x" . smex)))
  :config
  (progn
    (setq ido-enable-prefix nil
          ido-enable-flex-matching t
          ido-case-fold t
          ido-create-new-buffer 'always
          ido-use-filename-at-point nil
          ido-max-prospects 10)
    (add-to-list 'ido-ignore-files "\\.DS_Store")

    (defun my-ido-tweaks ()
      (define-key ido-file-completion-map
        (kbd "~")
        (lambda ()
          (interactive)
          (cond
           ((looking-back "~/") (insert "Projekty/"))
           ((looking-back "/") (insert "~/"))
           (:else (call-interactively 'self-insert-command)))))
      ;; C-n/p is more intuitive in vertical layout
      (define-key ido-completion-map (kbd "C-n") 'ido-next-match)
      (define-key ido-completion-map (kbd "<down>") 'ido-next-match)
      (define-key ido-completion-map (kbd "C-p") 'ido-prev-match)
      (define-key ido-completion-map (kbd "<up>") 'ido-prev-match))

    (add-hook 'ido-setup-hook 'my-ido-tweaks)))

(require 'use-package)

;;Latex
(use-package auctex
  :defer t
  :ensure t)

(use-package key-chord
  :ensure t
  :config
  (setq key-chord-two-keys-delay 0.05)
  (key-chord-define-global "x1" 'delete-other-windows)
  (key-chord-define-global "xk" 'ace-window)
  (key-chord-define-global "0o" 'delete-window)
  (key-chord-define-global "xg" 'magit-status)
  (key-chord-define-global "jf" 'ag-project))

(use-package use-package-chords
  :ensure t
  :config (key-chord-mode 1))

(defun jc/switch-to-previous-buffer ()
    "Switch to previously open buffer.
  Repeated invocations toggle between the two most recently open buffers."
    (interactive)
    (switch-to-buffer (other-buffer (current-buffer) 1)))

  (key-chord-define-global "JJ" 'jc/switch-to-previous-buffer)

(use-package ace-window
  :bind ("s-w" . ace-window)
  :config
  (setq aw-background nil)
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)))

(use-package avy
    :ensure t
    :chords (("jj" . avy-goto-char-2)
             ("jl" . avy-goto-line)))

(use-package ag
  :ensure t)

(use-package company
  :ensure t)

(use-package undo-tree
    :ensure t
    :chords (("uu" . undo-tree-visualize))
    :diminish undo-tree-mode:
    :config
    (global-undo-tree-mode 1))

;;magit-todos
(use-package async
  :ensure t)
(use-package dash
  :ensure t)
(use-package f
  :ensure t)
(use-package hl-todo
  :ensure t)
(use-package pcre2el
  :ensure t)
(use-package s
  :ensure t)

(require 'magit-todos)
(magit-todos-mode 1)

;;Org mode
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)
;; Set to the location of your Org files on your local system
(setq org-directory "~/org")
;; Set to the name of the file where new notes will be stored
(setq org-mobile-inbox-for-pull "~/org/flagged.org")
;; Set to <your Dropbox root directory>/MobileOrg.
(setq org-mobile-directory "~/Dropbox/Apps/MobileOrg")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#2e3436" "#a40000" "#4e9a06" "#c4a000" "#204a87" "#5c3566" "#729fcf" "#eeeeec"])
 '(custom-enabled-themes '(seti))
 '(custom-safe-themes
   '("1f6039038366e50129643d6a0dc67d1c34c70cdbe998e8c30dc4c6889ea7e3db" "01aeb538caf2b0ea9eed6451877d9adc13ba82e460774195bbe4cc83b70c8113" "d71f6c718dab453b625c407adc50479867a557668d5c21599a1ebea204d9e4f3" "3e6863b848fd3d9a066ed6130a7f1cf6d2ca1db201cc272968adccda326870d8" "3d5ef3d7ed58c9ad321f05360ad8a6b24585b9c49abcee67bdcbb0fe583a6950" "58c6711a3b568437bab07a30385d34aacf64156cc5137ea20e799984f4227265" "ed91d4e59412defda16b551eb705213773531f30eb95b69319ecd142fab118ca" "4c7a1f0559674bf6d5dd06ec52c8badc5ba6e091f954ea364a020ed702665aa1" "4af1f59f47f606bc63e078ad85900a6867c24a846c0b5d5a99c4efb26760a8ba" "4ba6aa8a2776688ef7fbf3eb2b5addfd86d6e8516a701e69720b705d0fbe7f08" "a0f33c18e1759db6be8ab33651a1d5a6da869942572112d3ab09b2d76bdf9c2e" default))
 '(display-buffer-reuse-frames t)
 '(fringe-mode 10 nil (fringe))
 '(linum-format " %6d ")
 '(magit-push-arguments nil)
 '(main-line-color1 "#222232")
 '(main-line-color2 "#333343")
 '(org-agenda-files '("~/org/moog-todo.org" "~/org/sylvan.org"))
 '(package-selected-packages
   '(protobuf-mode seti-theme danneskjold-theme company rainbow-delimiters magit sublime-themes evil smex ido-vertical-mode flx-ido projectile auctex exec-path-from-shell use-package))
 '(pop-up-windows nil)
 '(powerline-color1 "#222232")
 '(powerline-color2 "#333343")
 '(save-place t nil (saveplace)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
